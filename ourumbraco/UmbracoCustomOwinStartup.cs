﻿using Microsoft.Owin;
using Owin;
using Umbraco.Web;
using UmbracoV8;

[assembly: OwinStartup("UmbracoCustomOwinStartup", typeof(UmbracoCustomOwinStartup))]
namespace UmbracoV8
{
    public class UmbracoCustomOwinStartup : UmbracoDefaultOwinStartup
	{

		/// <summary>
		/// Configures the back office authentication for Umbraco
		/// </summary>
		/// <param name="app"></param>
		protected override void ConfigureUmbracoAuthentication(IAppBuilder app)
		{
			// Any connection or hub wire up and configuration should go here
			app.MapSignalR();
		}
	}
}